#!/bin/bash
export LOCAL_USER_ID=$((`id -u` > 999 ? `id -u` : 1010))
export LOCAL_GROUP_ID=$((`id -g` > 999 ? `id -g` : 1010))

echo "LOCAL_USER_ID = $LOCAL_USER_ID"
echo "LOCAL_GROUP_ID = $LOCAL_GROUP_ID"
