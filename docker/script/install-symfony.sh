#!/usr/bin/env bash

CONTAINER_NAME=synactroSymfonyWeb
CMD_DOCKER="docker exec -ti --user user ${CONTAINER_NAME}"

# First install
rm ../../src/.gitignore
${CMD_DOCKER} composer create-project symfony/skeleton /var/www
# or
#${CMD_DOCKER} composer create-project symfony/website-skeleton /var/www
echo "*" > ../../src/.gitignore
echo "!.gitignore" >> ../../src/.gitignore
# End first install

${CMD_DOCKER} composer require annotations
${CMD_DOCKER} composer require symfony/polyfill-php72
${CMD_DOCKER} composer require symfony/polyfill-php73

# Exemple
#${CMD_DOCKER} composer require guzzlehttp/guzzle:~6.0
#${CMD_DOCKER} composer require symfony/dom-crawler
#${CMD_DOCKER} composer require symfony/polyfill-php72
#${CMD_DOCKER} composer require symfony/cache
